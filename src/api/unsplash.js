import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com/',
    headers: {
        Authorization: 'Client-ID bb0666746a947f5842841d28cbbb9c64240dd43a266d9066698e5bdd744ee54b'
    }
});